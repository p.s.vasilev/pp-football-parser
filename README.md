# pp-football-parser

**Run locally**

- mvn clean install
- mvn spring-boot:run

**Example curl command**

- curl -X GET http://localhost:8080/parse -H "Content-Type: application/json" -d '{"input": "[PM] 0:00.000"}'
- curl -X GET http://localhost:8080/parse -H "Content-Type: application/json" -d '{"input": "FOO"}'

**Example curl commands for deployed app**

App is deployed to CF(Pivotal) - http://pb-football-parser-boring-lizard-yi.cfapps.io

FYI - limited resource so might crash not and not start back up.

- curl -X GET http://pb-football-parser-boring-lizard-yi.cfapps.io/parse -H "Content-Type: application/json" -d '{"input": "[PM] 0:00.000"}'
- curl -X GET http://pb-football-parser-boring-lizard-yi.cfapps.io/parse -H "Content-Type: application/json" -d '{"input": "FOO"}'