package com.pp.footballparser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FootballparserApplication {

	public static void main(String[] args) {
		SpringApplication.run(FootballparserApplication.class, args);
	}

}
