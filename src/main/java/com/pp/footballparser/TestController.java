package com.pp.footballparser;

import com.pp.footballparser.parser.FootballParser;
import com.pp.footballparser.response.RequestBodyFootballTime;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TestController {

    @GetMapping("/parse")
    public ResponseEntity<String> parse(@RequestBody RequestBodyFootballTime footballTime) {
        FootballParser parser = new FootballParser();
        return new ResponseEntity<>(parser.parse(footballTime.getInput()), HttpStatus.OK);
    }
}
