package com.pp.footballparser.parser;

import com.pp.footballparser.parser.impl.CalculateFootballTime;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FootballParser {

    private Optional<FootballTime> matchTime;

    private static String PATTERN_TIME_FORMAT = "^\\[(PM|FT|HT|H1|H2)\\]\\040([0-9]{1}|[0-9]{2}):([0-5]\\d)\\.([0-9]{3})$";

    public String parse(String input) {
        matchTime = toFootballTimeDTO(input);
        if (matchTime.isPresent()) {
            Optional<CalculateFootballTime> calculationPeriod = PeriodFactory.getPeriod(matchTime.get().getPeriod().toString());
            if (calculationPeriod.isPresent()) {
                String response = calculationPeriod.get().calculate(matchTime.get());
                return response;
            }
        }
        return "INVALID";
    }

    private Optional<FootballTime> toFootballTimeDTO(String input) {
        Matcher matcher = Pattern.compile(PATTERN_TIME_FORMAT).matcher(input);

        if (matcher.find()) {
            String period = matcher.group(1);
            String minutes = matcher.group(2);
            String seconds = matcher.group(3);
            String milliseconds = matcher.group(4);
            return Optional.of(new FootballTime(period, minutes, seconds, milliseconds));
        }

        return Optional.empty();
    }
}
