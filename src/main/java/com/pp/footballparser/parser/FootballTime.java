package com.pp.footballparser.parser;

public class FootballTime {

    private FootballTimeFormat period;
    private int minutes;
    private int seconds;
    private int milliseconds;

    public FootballTime(String period, String minutes, String seconds, String milliseconds) {
        this.period = FootballTimeFormat.valueOf(period);
        this.minutes = Integer.parseInt(minutes);
        this.seconds = Integer.parseInt(seconds);
        this.milliseconds = Integer.parseInt(milliseconds);
    }

    public FootballTimeFormat getPeriod() {
        return period;
    }

    public int getMinutes() {
        return minutes;
    }

    public int getSeconds() {
        return seconds;
    }

    public int getMilliseconds() {
        return milliseconds;
    }
}
