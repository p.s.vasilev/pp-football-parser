package com.pp.footballparser.parser;

public enum FootballTimeFormat {
    PM,
    H1,
    HT,
    H2,
    FT;
}
