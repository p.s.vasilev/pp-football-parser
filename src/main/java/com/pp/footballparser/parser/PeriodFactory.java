package com.pp.footballparser.parser;

import com.pp.footballparser.parser.impl.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class PeriodFactory {

    private PeriodFactory() {
        throw new IllegalStateException("Factory class");
    }

    private static Map<String, CalculateFootballTime> period = new HashMap<>();

    static {
        period.put(FootballTimeFormat.PM.toString(), new PMCalculation());
        period.put(FootballTimeFormat.H1.toString(), new H1Calculation());
        period.put(FootballTimeFormat.HT.toString(), new HTCalculation());
        period.put(FootballTimeFormat.H2.toString(), new H2Calculation());
        period.put(FootballTimeFormat.FT.toString(), new FTCalculation());
    }

    public static Optional<CalculateFootballTime> getPeriod(String period) {
        return Optional.ofNullable(PeriodFactory.period.get(period));
    }
}
