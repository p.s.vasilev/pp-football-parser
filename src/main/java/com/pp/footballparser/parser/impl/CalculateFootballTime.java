package com.pp.footballparser.parser.impl;

import com.pp.footballparser.parser.FootballTime;

public interface CalculateFootballTime {
    String calculate(FootballTime footballTime);
}
