package com.pp.footballparser.parser.impl;

import com.pp.footballparser.parser.FootballTime;
import com.pp.footballparser.parser.impl.CalculateFootballTime;

public class FTCalculation implements CalculateFootballTime {

    private static final String FOOTBALL_FT_OUTPUT = "90:00 +00:00 - FULL_TIME";
    @Override
    public String calculate(FootballTime footballTime) {
        return FOOTBALL_FT_OUTPUT;
    }
}
