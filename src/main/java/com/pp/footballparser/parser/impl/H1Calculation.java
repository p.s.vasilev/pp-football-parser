package com.pp.footballparser.parser.impl;

import com.pp.footballparser.parser.FootballTime;
import com.pp.footballparser.parser.impl.CalculateFootballTime;
import com.pp.footballparser.parser.utils.ExtraTime;
import com.pp.footballparser.parser.utils.MatchUtils;

public class H1Calculation implements CalculateFootballTime {

    private static final String FOOTBALL_H1_OUTPUT = "FIRST_HALF";

    @Override
    public String calculate(FootballTime time) {
        if (time.getMinutes() >= 45 && time.getMilliseconds() > 0) {
            ExtraTime extraTime = MatchUtils.calculateExtraTime(time);
            return String.format("45:00 +%02d:%02d - %s", extraTime.getMinutes(), extraTime.getSeconds(), FOOTBALL_H1_OUTPUT);
        }

        return String.format("%02d:%02d - %s", time.getMinutes(), MatchUtils.calculateSeconds(time), FOOTBALL_H1_OUTPUT);
    }
}
