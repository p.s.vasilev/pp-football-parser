package com.pp.footballparser.parser.impl;

import com.pp.footballparser.parser.FootballTime;
import com.pp.footballparser.parser.impl.CalculateFootballTime;

public class HTCalculation implements CalculateFootballTime {

    private static final String FOOTBALL_HALF_TIME_OUTPUT = "45:00 - HALF_TIME";

    @Override
    public String calculate(FootballTime footballTime) {
        return FOOTBALL_HALF_TIME_OUTPUT;
    }
}
