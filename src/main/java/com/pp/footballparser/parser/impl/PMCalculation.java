package com.pp.footballparser.parser.impl;

import com.pp.footballparser.parser.FootballTime;
import com.pp.footballparser.parser.impl.CalculateFootballTime;

public class PMCalculation implements CalculateFootballTime {

    private static final String FOOTBALL_FULL_TIME_OUTPUT = "00:00 - PRE_MATCH";

    @Override
    public String calculate(FootballTime footballTime) {
        return FOOTBALL_FULL_TIME_OUTPUT;
    }
}
