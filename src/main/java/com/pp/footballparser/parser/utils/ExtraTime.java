package com.pp.footballparser.parser.utils;

public class ExtraTime {

    private int minutes;
    private int seconds;

    public ExtraTime(int minutes, int seconds) {
        this.minutes = minutes;
        this.seconds = seconds;
    }

    public int getMinutes() {
        return minutes;
    }

    public int getSeconds() {
        return seconds;
    }
}
