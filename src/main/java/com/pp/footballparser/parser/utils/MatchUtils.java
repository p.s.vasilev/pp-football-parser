package com.pp.footballparser.parser.utils;

import com.pp.footballparser.parser.FootballTime;
import com.pp.footballparser.parser.FootballTimeFormat;

public class MatchUtils {

    private MatchUtils() {
        throw new IllegalStateException("Utility");
    }

    private static final int EXTRA = 1;

    public static ExtraTime calculateExtraTime(FootballTime footballTime) {
        if (footballTime.getMilliseconds() >= 500) {
            if (footballTime.getSeconds() + EXTRA == 60) {
                if (footballTime.getPeriod() == FootballTimeFormat.H1) {
                    return new ExtraTime(footballTime.getMinutes() + EXTRA - 45, 0);
                } else {
                    return new ExtraTime(footballTime.getMinutes() + EXTRA - 90, 0);
                }
            } else {
                if (footballTime.getPeriod() == FootballTimeFormat.H1) {
                    return new ExtraTime(footballTime.getMinutes() - 45, footballTime.getSeconds() + EXTRA);
                } else {
                    return new ExtraTime(footballTime.getMinutes() - 90, footballTime.getSeconds() + EXTRA);
                }
            }
        } else {
            if (footballTime.getPeriod() == FootballTimeFormat.H1) {
                return new ExtraTime(footballTime.getMinutes() - 45, footballTime.getSeconds());
            } else {
                return new ExtraTime(footballTime.getMinutes() - 90, footballTime.getSeconds());
            }
        }
    }

    public static int calculateSeconds(FootballTime footballTime) {
        if (footballTime.getMilliseconds() >= 500) {
            return footballTime.getSeconds() + 1;
        } else {
            return footballTime.getSeconds();
        }
    }
}
