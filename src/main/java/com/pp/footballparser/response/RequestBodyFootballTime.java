package com.pp.footballparser.response;

public class RequestBodyFootballTime {

    private String input;

    public RequestBodyFootballTime() {
    }

    public RequestBodyFootballTime(String input) {
        this.input = input;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }
}
