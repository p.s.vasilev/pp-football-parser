package com.pp.footballparser.parser;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.assertj.core.api.Assertions.assertThat;
class FootballParserTest {

    @Test
    @DisplayName("Test parser with expected input/output from code assignment")
    void parse() {
        FootballParser parser = new FootballParser();
        Map<String, String> map = new HashMap<>();
        map.put("[PM] 0:00.000", "00:00 - PRE_MATCH");
        map.put("[H1] 0:15.025", "00:15 - FIRST_HALF");
        map.put("[H1] 3:07.513", "03:08 - FIRST_HALF");
        map.put("[H1] 45:00.001", "45:00 +00:00 - FIRST_HALF");
        map.put("[H1] 46:15.752", "45:00 +01:16 - FIRST_HALF");
        map.put("[HT] 45:00.000", "45:00 - HALF_TIME");
        map.put("[H2] 45:00.500", "45:01 - SECOND_HALF");
        map.put("[H2] 90:00.908", "90:00 +00:01 - SECOND_HALF");
        map.put("[FT] 90:00.000", "90:00 +00:00 - FULL_TIME");
        map.put("90:00", "INVALID");
        map.put("[H3] 90:00.000", "INVALID");
        map.put("[PM] -10:00.000", "INVALID");
        map.put("FOO", "INVALID");

        map.forEach((k, v) -> assertEquals(v, parser.parse(k))
        );
    }

    @Test
    @DisplayName("[H1] 60:00.001 is a valid case too - assumption*")
    void hello() {
        FootballParser parser = new FootballParser();
        String input = "[H1] 60:00.001";
        String output = "45:00 +15:00 - FIRST_HALF";
        assertThat(parser.parse(input)).isEqualTo(output);
    }

    @Test
    @DisplayName("Assert regex is case sensitive")
    void regexCaseSensitive() {
        FootballParser parser = new FootballParser();
        String input = "[h1] 45:00.001";
        String output = "INVALID";
        assertThat(parser.parse(input)).isEqualTo(output);
    }
}