package com.pp.footballparser.parser.utils;

import com.pp.footballparser.parser.FootballTime;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.pp.footballparser.parser.FootballTimeFormat.H1;
import static com.pp.footballparser.parser.FootballTimeFormat.H2;
import static org.assertj.core.api.Assertions.assertThat;

class MatchUtilsTest {

    @Test
    @DisplayName("Calculate extra time when milliseconds is over/equal 500")
    void calculateSeconds_whenMilliseconds_over_or_equal_to_500() {
        String minutes = "45";
        String seconds = "10";
        String milliseconds = "500";
        FootballTime time = new FootballTime(H1.name(), minutes, seconds, milliseconds);

        int expectedSeconds = 11;

        assertThat(MatchUtils.calculateSeconds(time)).isEqualTo(expectedSeconds);
    }

    @Test
    @DisplayName("Calculate extra time when milliseconds is less than 500 - H2")
    void calculateSeconds_whenMilliseconds_is_less_than_500_and_H2_period() {
        String minutes = "90";
        String seconds = "10";
        String milliseconds = "499";
        FootballTime time = new FootballTime(H2.name(), minutes, seconds, milliseconds);

        int expectedSeconds = 10;

        assertThat(MatchUtils.calculateExtraTime(time)).isEqualToComparingFieldByField(new ExtraTime(0, expectedSeconds));
    }

    @Test
    @DisplayName("Calculate extra time when milliseconds is less than 500 - H1")
    void calculateSeconds_whenMilliseconds_is_less_than_500_and_H1_period() {
        String minutes = "45";
        String seconds = "10";
        String milliseconds = "499";
        FootballTime time = new FootballTime(H1.name(), minutes, seconds, milliseconds);

        int expectedSeconds = 10;

        assertThat(MatchUtils.calculateExtraTime(time)).isEqualToComparingFieldByField(new ExtraTime(0, expectedSeconds));
    }

    @Test
    @DisplayName("Calculate when period H1")
    void calculateExtraTime_when_period_H1() {
        String minutes = "45";
        String seconds = "40";
        String milliseconds = "500";
        FootballTime time = new FootballTime(H1.name(), minutes, seconds, milliseconds);

        int expectedSeconds = 41;
        int expectedMinutes = 0;

        assertThat(MatchUtils.calculateExtraTime(time)).isEqualToComparingFieldByField(new ExtraTime(expectedMinutes, expectedSeconds));
    }

    @Test
    @DisplayName("Calculate when period H2")
    void calculateExtraTime_when_period_H2() {
        String minutes = "90";
        String seconds = "40";
        String milliseconds = "500";
        FootballTime time = new FootballTime(H2.name(), minutes, seconds, milliseconds);

        int expectedSeconds = 41;
        int expectedMinutes = 0;

        assertThat(MatchUtils.calculateExtraTime(time)).isEqualToComparingFieldByField(new ExtraTime(expectedMinutes, expectedSeconds));
    }

    @Test
    @DisplayName("Calculate when period H2 and both seconds and minutes have extra time")
    void calculateExtraTime_when_period_H2_and_minutes_and_seconds_have_extra_time() {
        String minutes = "90";
        String seconds = "59";
        String milliseconds = "500";
        FootballTime time = new FootballTime(H2.name(), minutes, seconds, milliseconds);

        int expectedSeconds = 0;
        int expectedMinutes = 1;

        assertThat(MatchUtils.calculateExtraTime(time)).isEqualToComparingFieldByField(new ExtraTime(expectedMinutes, expectedSeconds));
    }

    @Test
    @DisplayName("Calculate when period H1 and both seconds and minutes have extra time")
    void calculateExtraTime_when_period_H1_and_minutes_and_seconds_have_extra_time() {
        String minutes = "45";
        String seconds = "59";
        String milliseconds = "500";
        FootballTime time = new FootballTime(H1.name(), minutes, seconds, milliseconds);

        int expectedSeconds = 0;
        int expectedMinutes = 1;

        assertThat(MatchUtils.calculateExtraTime(time)).isEqualToComparingFieldByField(new ExtraTime(expectedMinutes, expectedSeconds));
    }
}